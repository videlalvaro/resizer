(defproject resizer "0.1.0-SNAPSHOT"
  :description "Image Resizer for Cloudstagram"
  :url "http://cloudstagram.cloudfoundry.com/"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [org.clojure/data.json "0.1.3"]
                 [clojurewerkz/support "0.7.0-beta2"]
                 [com.novemberain/monger "1.1.2"]
                 [com.novemberain/langohr "1.0.0-beta4"]
                 [claude "0.2.1-SNAPSHOT"]
                 ]
  :plugins [[lein-swank "1.4.4"]]
  :main resizer.core
  :shell-wrapper true)
