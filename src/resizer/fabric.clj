(ns resizer.fabric
  (:require [claude.core :as cf]
            [claude.rabbitmq :as cfrmq]
            [langohr.exchange :as lhe]
            [langohr.core :as lhc]))

(defn rabbitmq-connect
  []
  (if (cf/cloudfoundry?)
    (lhc/connect (lhc/settings-from (cfrmq/url)))
    (lhc/connect)))

(defn declare-exchange
  [ch exchange]
  (let [{name :name type :type} exchange
        declare-ok (lhe/declare ch name type :durable true)]
    declare-ok))

(defn init-fabric
  [exchanges]
  (let [conn (rabbitmq-connect)
        ch   (.createChannel conn)]
    (doseq [exchange exchanges]
      (declare-exchange ch exchange))
    (.close ch)
    (.close conn)))