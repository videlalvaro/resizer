(ns resizer.core
  (:gen-class)
  (:use [resizer.consumer :as rc]
        [resizer.resizer :as rz]
        [resizer.fabric :as rf]))

(def exchanges [{:name "cloudstagram-broadcast-newimage" :type "fanout"}
                {:name "cloudstagram-new-image" :type "direct"}
                {:name "cloudstagram-upload" :type "direct"}])

(defn -main
  []
  (println "Connecting to Mongodb")
  (rz/mongo-connect)
  (println "Initializing Fabric")
  (rf/init-fabric exchanges)
  (println "Starting RabbitMQ Consumers")
  (rc/start-consumer)
  (println "App Running"))